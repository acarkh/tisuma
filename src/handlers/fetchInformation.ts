import { green } from "std/fmt/colors.ts"

import { getApplicationInformation } from "x/discordeno/mod.ts"

import store from "../helpers/store.ts"

import Handler from "../types/Handler.ts"

const fetchInformation: Handler = {
  name: "info",
  for: "ready",
  priority: 3,
  execute: (async () => {
    let repository
    const repositoryID = Deno.env.get("GITLAB_PROJECT_ID") as string

    const application = await getApplicationInformation()
    store.variables.set("application", application)
    console.log(`Fetched and cached ${green("application data")} from ${green("Discord")}.`);

    if (repositoryID) {
      repository = await fetch(`https://gitlab.com/api/v4/projects/${repositoryID}`).then((response) => response.json())
      repository.commits = await fetch(`https://gitlab.com/api/v4/projects/${repositoryID}/repository/commits`).then((response) => response.json())
      repository.contributors = await fetch(`https://gitlab.com/api/v4/projects/${repositoryID}/repository/contributors`).then((response) => response.json())
      store.variables.set("repository", repository)

      console.log(`Fetched and cached ${green("repository data")} from ${green("GitLab")}.`);
    }
  })
}

export default fetchInformation
