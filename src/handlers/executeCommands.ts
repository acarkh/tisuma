import { Message } from "x/discordeno/mod.ts"

import store from "../helpers/store.ts"
import { duration } from "../helpers/date.ts"

import Handler from "../types/Handler.ts"
import Command from "../types/Command.ts"

const cooldownSet = new Map()

const executeCommands: Handler = {
  name: "commands",
  for: "messageCreate",
  priority: 3,
  execute: (async (message: Message) => {
    const prefix = Deno.env.get("PREFIX") as string

    if(message.content.startsWith(prefix)) {
      const args = message.content.slice(prefix.length).split(" ")
      const input = args.pop() as string
      const commands = store.commands.array()

      if(commands.some((command) => command.name === input || command.aliases.includes(input))) {
        const command = commands.find((command) => command.name === input || command.aliases.includes(input)) as Command
        const cooldown = command.cooldown
        const filters = command.filters
        const authorID = message.author.id

        if(cooldownSet.has(authorID)) {
          const lastUsed = cooldownSet.get(authorID)

          if(lastUsed < Date.now()) {
            cooldownSet.delete(authorID)
          } else {
            message.reply(`You can use this command after ${duration(lastUsed - Date.now(), true)}.`)
            return
          }
        }

        if(filters.length !== 0) {
          const filtersLength = filters.length
          let totallyPassed = true

          for(let i = 0; i < filtersLength; i++) {
            const filter = filters[i]
            const passed = filter.apply(message)

            if(!passed) {
              message.reply(`You couldn't pass ${filter.name} filter: ${filter.description}`)
              totallyPassed = false
              break
            }
          }

          if(!totallyPassed) return
        }

        cooldownSet.set(authorID, Date.now() + (cooldown * 1000))

        await command.execute(message, args)
      }
    }
  })
}

export default executeCommands
