import { stripIndents } from "x/deno_tags/tags/stripIndents.ts"

import store from "../helpers/store.ts"
import humanizeString from "../helpers/humanizeString.ts"

import Command from "../types/Command.ts"
import Categories from "../enums/Categories.ts"

const Commands: Command = {
  name: "commands",
  description: "Get information about the commands of tisuma.",
  aliases: ["help"],
  category: Categories.INFORMATION,
  cooldown: 5,
  filters: [],
  execute: (async (message) => {
    const contentArray = []

    let categoryNames = Object.keys(Categories) // ["index", "index_2", "value", "value_2"]
    categoryNames = categoryNames.slice(categoryNames.length / 2).map((categoryName) => humanizeString(categoryName))
    const categoryNamesLength = categoryNames.length

    const commands = store.commands.array()
    const commandsLength = commands.length
    const sortedByCategories: {
      [categoryName: string]: string[]
    } = {}

    for (let i = 0; i < commandsLength; i++) {
      const command = commands[i] as Command
      const categoryName = categoryNames[command.category]

      if (!(categoryName in sortedByCategories)) {
        sortedByCategories[categoryName] = []
      }

      sortedByCategories[categoryName].push(command.name)
    }

    for (let i = 0; i < categoryNamesLength; i++) {
      const categoryName = categoryNames[i]

      if (sortedByCategories[categoryName]) {
        contentArray.push(stripIndents`
          **+** ${categoryName}

          ${sortedByCategories[categoryName].map((commandName) => `\`${commandName}\``).join(" **-** ")}
        `)
      }
    }

    message.reply(contentArray.join("\n\n\n"))
  })
}

export default Commands
