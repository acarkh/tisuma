import { VERSION } from "std/version.ts"

import { cache, avatarURL, Member, Embed } from "x/discordeno/mod.ts"
import { stripIndents } from "x/deno_tags/tags/stripIndents.ts"

import store from "../helpers/store.ts"
import createBlankField from "../helpers/createBlankField.ts"
import removeBreakLines from "../helpers/removeBreakLines.ts"

import Command from "../types/Command.ts"
import Categories from "../enums/Categories.ts"

const Information: Command = {
  name: "info",
  description: "Get information about tisuma.",
  aliases: ["information", "stats", "about"],
  category: Categories.INFORMATION,
  cooldown: 3,
  filters: [],
  execute: (async (message, args) => {
    const repository = store.variables.get("repository")
    const discordApp = store.variables.get("application")
    const botOwner = cache.members.get(discordApp.owner.id as string) as Member
    const blankField = createBlankField()
    const memoryUsage = Deno.memoryUsage()
    const supportCode = Deno.env.get("SUPPORT")

    const embed: Embed = {
      description: discordApp.description,
      color: 0x00FF00,
      fields: [
        {
          name: "Store",
          value: removeBreakLines(stripIndents`
            **${store.commands.size}** commands
            **${store.events.size}** events
            **${store.handlers.size}** handlers
          `),
          inline: true
        },
        blankField,
        {
          name: "Cache",
          value: (stripIndents`
            **${cache.guilds.size}** servers
            **${cache.members.size}** users
            **${cache.channels.size}** channels
          `),
          inline: true
        },
        {
          name: "Links",
          value: removeBreakLines(stripIndents`
            [Invite me!](https://discord.com/oauth2/authorize?client_id=${discordApp.id}&scope=bot&permissions=8)
            ${supportCode ? `[Join my support server!](https://discord.gg/${supportCode})` : ""}
            ${repository ? `[Look at my source code!](https://gitlab.com/${repository.path_with_namespace})` : ""}
          `),
          inline: true
        },
        blankField,
        {
          name: "Memory",
          value: (stripIndents`
            Free: **${((memoryUsage.heapTotal - memoryUsage.heapUsed) / 1024 / 1024).toFixed(2)} MB**
            Using: **${(memoryUsage.heapUsed / 1024 / 1024).toFixed(2)} MB**
            Allocated: **${(memoryUsage.heapTotal / 1024 / 1024).toFixed(2)} MB**
          `),
          inline: true
        }
      ]
    }

    if (repository) {
      embed.footer = {
        text: `${repository.contributors.length} contributors and ${repository.star_count} stars on GitLab`
      }
    }

    message.reply({ embed })
  })
}

export default Information
