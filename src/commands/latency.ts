import store from "../helpers/store.ts"

import Command from "../types/Command.ts"
import Categories from "../enums/Categories.ts"

const Latency: Command = {
  name: "latency",
  description: "Get information about the latency of tisuma.",
  aliases: ["ping"],
  category: Categories.UTILITY,
  cooldown: 4,
  filters: [],
  execute: (async (message) => {
    message.reply(`My latency is **${store.variables.get("latency") || "(not calculated yet)"}ms**.`)
  })
}

export default Latency
