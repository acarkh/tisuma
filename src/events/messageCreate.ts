import Event from "../types/Event.ts"

const messageCreate: Event = {
  name: "messageCreate"
}

export default messageCreate
