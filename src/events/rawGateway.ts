import store from "../helpers/store.ts"

const rawGateway = {
  name: "rawGateway",
  execute: (async (data: any) => {
    if (data.op === 11) {
      store.variables.set("latency", Date.now() - store.variables.get("heartbeatAt"))
      store.variables.delete("heartbeatAt")
    }
  })
}

export default rawGateway
