import { DebugArg } from "x/discordeno/mod.ts"

import store from "../helpers/store.ts"

const debug = {
  name: "debug",
  execute: (async (content: DebugArg) => {
    if(content.type === "gatewayHeartbeat") {
      store.variables.set("heartbeatAt", Date.now())
    }
  })
}

export default debug
