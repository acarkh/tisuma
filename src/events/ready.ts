import { green } from "std/fmt/colors.ts"

const ready = {
  name: "ready",
  execute: (async () => {
    console.log(`Successfully ${green("connected")} to gateway.`)
  })
}

export default ready
