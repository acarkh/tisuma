import { Message } from "x/discordeno/mod.ts"

import Categories from "../enums/Categories.ts"

interface CommandFilter {
  name: string
  description: string
  apply(message: Message): boolean
}

interface Command {
  name: string
  description: string
  aliases: string[]
  category: Categories
  cooldown: number
  filters: CommandFilter[]
  execute(message: Message, args: string[]): (void | Promise<void>)
}

export default Command
