interface Handler {
  name: string
  for: string
  priority: (0 | 1 | 2 | 3)
  execute(...args: any[]): (void | Promise<void>)
}

export default Handler
