const createBlankField = ((inline: boolean = true) => {
  const field = {
    name: "\u200B",
    value: "\u200B",
    inline
  }

  return field
})

export default createBlankField
