import { green } from "std/fmt/colors.ts"

const loadEnv = (async (): Promise<void> => {
  const decoder = new TextDecoder("utf-8")
  const data = await Deno.readFile("./.env")
  const content = decoder.decode(data)
  const lines = content.split("\n").filter((line) => line !== "")
  const lineCount = lines.length

  for (let i = 0; i < lineCount; i++) {
    const line = lines[i]
    const splitted = line.split("=")

    if (!splitted[1]) splitted[1] = ""
    if (splitted[1].endsWith("\r")) splitted[1] = splitted[1].slice(0, -1)

    Deno.env.set(splitted[0], splitted[1])
  }

  console.log(`${green("Environment variables")} is loaded.`)
});

export default loadEnv
