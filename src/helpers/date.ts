export const duration = ((timestamp: number, nano: boolean = false): string => {
  let seconds = 0
  let minutes = 0
  let hours = 0
  let days = 0
  let months = 0
  let years = 0
  let f: string[] = []

  seconds = nano ? timestamp / 1000 : (timestamp - (timestamp % 1000)) / 1000

  minutes = (seconds - (seconds % 60)) / 60
  seconds = seconds - (minutes * 60)

  hours = (minutes - (minutes % 60)) / 60
  minutes = minutes - (hours * 60)

  days = (hours - (hours % 24)) / 24
  hours = hours - (hours * 24)

  months = (days - (days % 30)) / 30
  days = days - (days * 30)

  years = (months - (months % 12)) / 12
  months = months - (months * 12)

  if(years) f.push(`${years} years`)
  if(months) f.push(`${months} months`)
  if(days) f.push(`${days} days`)
  if(hours) f.push(`${hours} hours`)
  if(minutes) f.push(`${minutes} minutes`)
  if(seconds) f.push(`${seconds} seconds`)

  return f.join(" ")
})

