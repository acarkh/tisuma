import Collection from "./Collection.ts"

import Command from "../types/Command.ts"
import Event from "../types/Event.ts"
import Handler from "../types/Handler.ts"

const store = {
  commands: new Collection<string, Command>(),
  events: new Collection<string, Event>(),
  handlers: new Collection<string, Handler>(),
  variables: new Collection<string, any>()
}

export default store
