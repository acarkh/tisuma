import Collection from "./Collection.ts"

const loadFolder = (async (folderName: string, collection: Collection<string, any>) => {
  const iterator = Deno.readDirSync(`./src/${folderName}`)
  const data = [...iterator]
  const dataLength = data.length

  for (let i = 0; i < dataLength; i++) {
    const file = data[i]

    if (file.isFile && file.name.endsWith(".ts")) {
      const denoModule = (await import(`../${folderName}/${file.name}`)).default as { [key: string]: any }

      if (denoModule.name && typeof denoModule.name === "string") {
        collection.set(denoModule.name as string, denoModule)
      } else {
        collection.set(file.name.slice(0, -3), denoModule)
      }
    }
  }
})

export default loadFolder
