import { green } from "std/fmt/colors.ts"

import { EventHandlers } from "x/discordeno/mod.ts"

import Collection from "./Collection.ts"

import Handler from "../types/Handler.ts"
import Event from "../types/Event.ts"

const setupEvents = ((events: Collection<string, Event>, handlers: Collection<string, Handler>): EventHandlers => {
  const ret = {} as { [key: string]: any }

  const eventObject = events.object()
  const eventNames = Object.keys(eventObject)

  const handlerArray = handlers.array()

  for (let a = 0; a < eventNames.length; a++) {
    const event = events.get(eventNames[a]) as Event
    const sortedHandlers = handlerArray.filter((handler) => handler.for === event.name).sort((c, d) => d.priority - c.priority)

    ret[event.name] = (async (...args: any[]) => {
      for (let b = 0; b < sortedHandlers.length; b++) {
        await sortedHandlers[b].execute(...args)
      }

      if (event.execute) {
        await event.execute(...args)
      }
    })

    console.log(`Loaded ${green(event.name)} event and its handlers.`)
  }

  return ret as EventHandlers
})

export default setupEvents
