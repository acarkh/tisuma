import { Collection as DiscordenoCollection } from "x/discordeno/src/util/collection.ts"

class Collection<K, V> extends DiscordenoCollection<string, V> {
  object() {
    const keys = [...this.keys()]
    const values = this.array()
    const ret: Record<string, V> = {}

    for (let i = 0; i < keys.length; i++) {
      ret[keys[i] as string] = values[i]
    }

    return ret
  }
}

export default Collection
