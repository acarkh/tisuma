const removeBreakLines = ((data: string): string => {
  return data.split("\n").filter((line) => line !== "").join("\n")
})

export default removeBreakLines
