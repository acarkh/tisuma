import { startBot } from "x/discordeno/mod.ts"

import store from "./helpers/store.ts"
import loadEnv from "./helpers/loadEnv.ts"
import loadFolder from "./helpers/loadFolder.ts"
import setupEvents from "./helpers/setupEvents.ts"

await loadEnv()
await loadFolder("commands", store.commands)
await loadFolder("events", store.events)
await loadFolder("handlers", store.handlers)

const events = setupEvents(store.events, store.handlers)

startBot({
  token: Deno.env.get("TOKEN") as string,
  intents: ["GUILDS", "GUILD_MESSAGES"],
  eventHandlers: events
})
