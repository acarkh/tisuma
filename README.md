# tisuma
A basic bot made for Discord.

## Motivation
I started to coding with discord bots, I did some packages, applications, web applications etc. but I didn't continue these. I want to continue tisuma. Also Discord bots at least the ones i used aren't good for me.
I want to make best Discord bot.

## Contributing
See [CONTRIBUTING](https://gitlab.com/acarkh/tisuma/-/blob/master/CONTRIBUTING) for how to contribute.

## License
This project licensed under the MIT license. See [LICENSE](https://gitlab.com/acarkh/tisuma/-/blob/master/LICENSE) for more information.
